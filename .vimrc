set encoding=utf-8

set number	
set linebreak	
set showbreak=+++
set textwidth=100	
set showmatch	

syntax on

set nocompatible
filetype off

set hlsearch	
set smartcase	
set ignorecase
set incsearch
 
set autoindent	
set shiftwidth=4	
set smartindent	
set smarttab	
set softtabstop=4
 
set ruler	
 
set undolevels=1000	
set backspace=indent,eol,start	

set lazyredraw

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-surround'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'junegunn/goyo.vim'
Plugin 'lervag/vimtex'
let g:airline_theme='minimalist'
call vundle#end()

set guifont=Iosevka\ 12
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

filetype plugin indent on

