#!/bin/bash

# Global tags
set -e -u -x

# Fulfilling requirements:
# Installing stow, pipx and gnome-extensions-cli
if ! type stow; then
    dnf install stow
fi

if ! type pipx; then
    dnf install pipx -y
fi

if ! type gnome-extensions-cli; then
    pipx install gnome-extensions-cli
    ~/.local/pipx/venvs/gnome-extensions-cli/bin/python -m pip install pygobject
fi

install_extension () {
    gnome-extensions-cli install "$1"
}

# To be configured: using stow to move files

# insert # 

# Installing key extensions
install_extension appindicatorsupport@rgcjonas.gmail.com
install_extension blur-my-shell@aunetx
install_extension caffeine@patapon.info
install_extension gnome-shell-screenshot@ttll.de
install_extension gsconnect@andyholmes.github.io
install_extension just-perfection-desktop@just-perfection
install_extension sound-output-device-chooser@kgshank.net
install_extension timepp@zagortenay333

# Installation of GNOME settings with dconf:
# https://peterbabic.dev/blog/keep-gnome-shell-settings-dotfiles-yadm/; dump is being created
# by dconf dump / > gnome-settings.ini
# # dconf load / < gnome-settings.ini
